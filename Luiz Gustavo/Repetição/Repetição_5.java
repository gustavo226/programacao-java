import java.util.Scanner;

public class Repeti��o_5 {

	static Scanner tecla = new Scanner(System.in);
	
		public static void main(String[] args) {
		
			double i, j;
			
			System.out.println("Informe a primeira nota: ");
			i = tecla.nextDouble();
			while (i < 0 || i > 10) {
				System.out.println("Nota Inv�lida!");
				i = tecla.nextDouble();
			}
			
			System.out.println("Informe a segunda nota: ");
			j = tecla.nextDouble();
			while (j < 0 || j > 10) {
				System.out.println("Nota Inv�lida!");
				j = tecla.nextDouble();
			}
			
			System.out.println("M�dia: " + (i+j)/2);
			
		}
}
