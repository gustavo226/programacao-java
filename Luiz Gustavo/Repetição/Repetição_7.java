import java.util.Scanner;

public class Repeti��o_7 {

	static Scanner tecla = new Scanner(System.in);
	
		public static void main(String[] args) {
			
			double i, j;
			int op;
			
			do {
			
			System.out.println("Informe a primeira nota: ");
			i = tecla.nextDouble();
			while (i < 0 || i > 10) {
				System.out.println("Nota Inv�lida!");
				i = tecla.nextDouble();
			}
			
			System.out.println("Informe a segunda nota: ");
			j = tecla.nextDouble();
			while (j < 0 || j > 10) {
				System.out.println("Nota Inv�lida!");
				j = tecla.nextDouble();
			}
			
			System.out.println("M�dia: " + (i+j)/2);
			
			System.out.println("\nNovo c�lculo?");
			System.out.println("Digite (1) para sim.");
			System.out.println("Digite (2) para n�o.");
			op = tecla.nextInt();
			while (op != 1 && op != 2) {
				System.out.println("Digite apenas uma das duas op��es.");
				op = tecla.nextInt();
			}
			
		} while (op == 1);
			
			if (op == 2) {
				System.out.println("Programa encerrado.");
			}
	
		}
		
}
