import java.util.Scanner;

public class Repeti��o_10 {
	
	static Scanner tecla = new Scanner(System.in);

		public static void main(String[] args) {
		
			double av1, av2, media;
			char op;
			int apr=0; 
			int rep=0;
			int exame=0;
			
			do {
			
				System.out.println("Digite as notas: ");
				System.out.println("\nAv1: ");
				av1 = tecla.nextDouble();
				System.out.println("Av2: ");
				av2 = tecla.nextDouble();
			
				media = (av1+av2)/2;
			
				System.out.println("M�dia: " + media );
				System.out.println("Calcular a m�dia de outro aluno [S]im [N]�o?");
				op = tecla.next().charAt(0);
			
				while (op != 'S' && op != 'N') {
					System.out.println("Letra Inv�lida!");
					op = tecla.next().charAt(0); 
				}
			
				if (media >= 6) {
					apr = apr + 1;
				}
			
				if (media < 3) {
					rep = rep +1;
				}
			
				if (media >= 3 && media < 6) {
					exame = exame + 1;
				}
			
			
			} while (op == 'S');
			
			System.out.println("Total de alunos aprovados: " + apr);
			System.out.println("Total de alunos reprovados: " + rep);
			System.out.println("Total de alunos de exame: " + exame);
			
		}
}