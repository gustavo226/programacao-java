
public class Vetor_3 {
	
	static int[] B = new int [10];

	public static void main(String[] args) {
		
		int i;
		
		for (i = 0; i < B.length ; i++) {
			if (i % 2 == 0) {
				B[i] = 0;
			} else {
				B[i] = 1;
			}	
		}
		
		for (i = 0; i < B.length; i++) {
			System.out.println(B[i]);
		}
	}
}